($(document).ready(function() {
    $('div[data-src],a[data-src]').each(function() {
        setElementBackground.apply(this);
    });

    function setElementBackground() {
        var elem = $(this);
        var src = elem.data('src').split(',');
        var result = '';
        if (src.length >= 2) {
            for (var i = 0; i < src.length; i++) {
                if (i === src.length - 1) {
                    result += ' url(' + src[i] + ')';
                } else {
                    result += ' url(' + src[i] + '),';
                }
            }
        } else {
            result = ' url(' + src[0] + ')';
        }

        elem.css('background-image', result);
    }
}))